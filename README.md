# Vagrant-Ansible environment for Windows

Provides a virtual server Ubuntu 16.04 LTS usign Vagrant and Ansible.

Packages availables:

  * Apache2
  * PHP 7
  * MySQL
  * Composer

## Prerequisites

  * VirtualBox
  * Vagrant

## Install

After you have installed the prerequisites softwares, just clone this repository on the root of your Symfony project.

    $ cd your_project
    $ git@bitbucket.org:fsuarezm/vagrant-windows.git vagrant

Copy the ```parameters.yml.dist``` file into ```parameters.yml``` file

    $ cp vagrant/parameters.yml.dist vagrant/parameters.yml

Modify your Parameters file upon your wishes and then run vagrant:

    $ cd vagrant
    $ vagrant up --provision

## Configuration

This project is pre-configured to install an Ubuntu 16.04 eith PHP, MySQL, Apache2 and phpmyadmin. It also create a network
interface with the IP 10.10.10.10

MySQL user: ```root``` password: ```root```

Automatically setups database with this setup:

    * Username: symfony
    * Password: symfony
    * Database: symfony

For default debug symfony project, change ```app.php```

        $env = getenv('APP_ENV') ? getenv('APP_ENV') : 'prod';
        $debug = (bool)getenv('APP_DEBUG');
        $kernel = new AppKernel($env, $debug);

Enjoy!
Feel free to contribute to this project ;)
