# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	# Look for project variables file.
	if !(File.exists?(File.dirname(__FILE__) + "/parameters.yml"))
		raise NoVarsException
	end
	
	# Load the yml files
	require 'yaml'
	vars = YAML.load_file(File.dirname(__FILE__) + "/parameters.yml")
	#vars = YAML.load_file(File.dirname(__FILE__) + "/global.yml")
	#vars.merge!(YAML.load_file(File.dirname(__FILE__) + "/parameters.yml"))
	
	# Read this user's host machine's public ssh key to pass to ansible.
	if !(File.exists?("#{Dir.home}/.ssh/id_rsa.pub"))
		raise NoSshKeyException
	end
	ssh_public_key = IO.read("#{Dir.home}/.ssh/id_rsa.pub").strip!

    config.vm.box = vars['base_box']
    config.vm.network :private_network, ip: vars['private_ip']
    config.vm.network :forwarded_port, host: 8000, guest: 80
    config.ssh.forward_agent = true
    config.vm.synced_folder "../", vars['vm_sync_folder'], id: "vagrant-root", :nfs => true

    config.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--memory", vars['vm_memory']]
      v.customize ["modifyvm", :id, "--name", vars['vm_name']]
    end
	
	# Windows workaround, install it in the guest and run the playbook there.
    config.vm.provision "shell", path: File.dirname(__FILE__) + "/scripts/internal-ansible.sh"
	
	# Run ansible Provisioner via shell.
    config.vm.provision "shell",
      inline: "ansible-playbook -c local  -i 'default,' #{vars['playbook']} --extra-vars 'authorized_keys=\"#{ssh_public_key}\"'"
end

##
# Our Exceptions
#
class NoVarsException < Vagrant::Errors::VagrantError
	error_message('Project variables file not found. Copy parameters.dist.yml to parameters.yml, edit to match your project, then try again.')
end

class NoSshKeyException < Vagrant::Errors::VagrantError
	error_message('An ssh public key could not be found at ~/.ssh/id_rsa.pub. Please generate one and try again.')
end

class NoSSHException < Vagrant::Errors::VagrantError
	error_message('Your SSH does not currently contain any keys (or is stopped.)')
	error_message('If you are on a Mac, add a passphrase to your SSH key by running:')
	error_message('  $ ssh-keygen -p -f ~/.ssh/id_rsa')
	error_message('  $ eval $(ssh-agent)')
	error_message('  $ ssh-add')
end
